# Checks the lenthg of String

course = "Python for Beginners"
course2 = "python for Beginners"
print("Lenght of string: " + str(len(course)))

# Function not belong to Strings
# Methods are done on Strings

print(course2.capitalize()) # first letter capitalized
print(course.upper())
print(course.lower())
print(course.find("t"))
#  output 11 cause starts with 11th character
print(course.find("Begi"))
#  output -1 cause not found
print(course.find("Bego"))
print(course.replace("Beginners","Absolute beginners"))
print(course.replace("P","J"))
# find Python in course variable boolean return
print(("Python" in course))