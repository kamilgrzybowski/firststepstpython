cours = "Python for Beginers"
cours2 = "Python's cours"
cours3 = "Python's cours 'Python for beginners'"
print(cours)
print(cours2)
print(cours3)
print()
email = """     Let's try this e-mail space for multilines string
    This is second line of same string
    And this is another line
 
 
    Here is bottom of this e-mail space
    Cheers!
 """

print(email)

print()
print(cours[0])
print(cours[-5])
print(cours[0:4])
print()

firstName = "Joe"
lastName = "Black"
msg = f"{firstName} [{lastName}] will be a coder"
print(msg)