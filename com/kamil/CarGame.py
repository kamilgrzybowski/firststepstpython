print("Car game.")
print("Type 'help' to see guide.")

command = ""
is_Stopped = True

while True:
    command = input(">").lower() #DRY principle
    if command == "start":
        if is_Stopped:
            print("Car started...")
            is_Stopped = False
        else:
            print("Car already started!")
    elif command == "stop":
        if not is_Stopped:
            print("Car stopped...")
            is_Stopped = True
        else:
            print("Car already stopped!")
    elif command == "help":
        print("""
start - to start the car
stop - to stop the car
quit - to quit the game
        """)
    elif command == "quit":
        print("Game over")
        break
    else:
        print("Sorry, I don't understand a command...")