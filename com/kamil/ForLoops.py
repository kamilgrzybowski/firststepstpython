for item in ["John", "Tom", "Sarah", "Brad"]:
    print(item)

print("---------------------")

for item in range(5, 10):
    print(item)

print("---------------------")

for item in range(5,20,2):
    print(item)

print("---------------------")

costs = 0
for x  in [10,20,30,50,128]:
    costs += x
print(f"Total costst: {costs}")

print("---------------------")

counter = 0
for number in range(1,100):
    counter += number

print(f"Total: {counter}")

print("------------")

list = ["Tom", "Bill", "Mark", "Mike"]
print(list[:-1])