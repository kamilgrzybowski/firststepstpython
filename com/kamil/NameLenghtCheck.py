name = "Ola"

if len(name) < 3:
    print("Name seems to be to short")
    print("Please provide correct name")
elif len(name) > 50:
    print("Name seems to be to long")
    print("Please provide correct name")
else:
    print("Name looks good!")
    print(f"Hello {name}! Have a nice day")
