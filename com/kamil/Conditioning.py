isHot = False

if isHot:
    print("It's a hot day")
    print("Have a nice day outside")
else:
    print("It's cold outside")
    print("Stay at home bro")


print()

has_high_income = True
has_good_credit = False
has_criminal_record = False

if has_high_income and has_good_credit and not has_criminal_record:
    print("Eligible for loan")
elif has_high_income or has_good_credit and not has_criminal_record:
    print("Needs further consideration")
else:
    print("Conditions not reached for loan")
